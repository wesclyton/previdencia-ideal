module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable('agendamentos', {
      id_agendamento: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      data: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      servico: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      link: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      estado: {
        type: Sequelize.CHAR(2),
        allowNull: false,
        defaultValue: false,
      },
      id_pagamento: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: 'pagamentos', key: 'id_pagamento' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      id_pessoa: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'pessoas', key: 'id_pessoa' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
    });
  },
  down: async (queryInterface) => {
    queryInterface.dropTable('agendamentos');
  },
};
