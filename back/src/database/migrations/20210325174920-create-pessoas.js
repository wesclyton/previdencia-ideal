module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable('pessoas', {
      id_pessoa: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      nome: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(150),
        allowNull: false,
        unique: true,
      },
      telefone: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      telefone2: {
        type: Sequelize.STRING(20),
        allowNull: true,
      },
      sexo: {
        type: Sequelize.CHAR(1),
        allowNull: false,
      },
      cpf: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      cnh: {
        type: Sequelize.STRING(20),
        allowNull: true,
      },
      nascimento: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      profissao: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      cep: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      endereco: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      numero: {
        type: Sequelize.STRING(10),
        allowNull: false,
      },
      bairro: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      ativo: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      id_cidade: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'cidades', key: 'id_cidade' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      id_patrocinador: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { model: 'pessoas', key: 'id_pessoa' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      id_usuario: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'usuarios', key: 'id_usuario' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
    });
  },

  down: async (queryInterface) => {
    queryInterface.dropTable('pessoas');
  },
};
