module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable('respostas', {
      id_resposta: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      resposta: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      ativo: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      id_pessoa: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'pessoas', key: 'id_pessoa' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      id_pergunta: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'perguntas', key: 'id_pergunta' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
      uppdated_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
    });
  },

  down: async (queryInterface) => {
    queryInterface.dropTable('respostas');
  },
};
