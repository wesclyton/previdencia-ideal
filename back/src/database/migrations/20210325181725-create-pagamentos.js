module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable('pagamentos', {
      id_pagamento: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      tipo: {
        type: Sequelize.CHAR(1),
        allowNull: false,
      },
      token: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      titulo: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      valor: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false,
      },
      parcelas: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      forma_pagamento: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      estado: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
    });
  },
  down: async (queryInterface) => {
    queryInterface.dropTable('pagamentos');
  },
};
