module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable('cidades', {
      id_cidade: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      nome: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      uf: {
        type: Sequelize.CHAR(2),
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
    });
  },
  down: async (queryInterface) => {
    queryInterface.dropTable('cidades');
  },
};
