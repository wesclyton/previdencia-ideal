const bcrypt = require('bcryptjs');

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert(
      'usuarios',
      [
        {
          tipo: 'A',
          email: 'contato@previdenciaideal.com',
          username: 'ramos',
          password_hash: bcrypt.hashSync('123456', 8),
        },
        {
          tipo: 'A',
          email: 'wes.hinsch@gmail.com',
          username: 'weslley',
          password_hash: bcrypt.hashSync('123456', 8),
        },
        {
          tipo: 'P',
          email: 'parceiro01@email.com',
          username: 'parceiro01',
          password_hash: bcrypt.hashSync('123456', 8),
        },
        {
          tipo: 'P',
          email: 'parceiro02@email.com',
          username: 'parceiro02',
          password_hash: bcrypt.hashSync('123456', 8),
        },
        {
          tipo: 'C',
          email: 'cliente01@email.com',
          username: 'cliente01',
          password_hash: bcrypt.hashSync('123456', 8),
        },
        {
          tipo: 'C',
          email: 'cliente02@email.com',
          username: 'cliente02',
          password_hash: bcrypt.hashSync('123456', 8),
        },
        {
          tipo: 'C',
          email: 'cliente03@email.com',
          username: 'cliente03',
          password_hash: bcrypt.hashSync('123456', 8),
        },
        {
          tipo: 'C',
          email: 'cliente04@email.com',
          username: 'cliente04',
          password_hash: bcrypt.hashSync('123456', 8),
        },
      ],
      {}
    );
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('usuarios', null, {});
  },
};
