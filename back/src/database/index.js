import Sequelize from 'sequelize';
import Cidade from '../app/models/Cidade';
import Pessoa from '../app/models/Pessoa';
import Usuario from '../app/models/Usuario';
import Pagamento from '../app/models/Pagamento';
import Agendamento from '../app/models/Agendamento';
import Parceiro from '../app/models/Parceiro';
import Log from '../app/models/Log';
import ChangePassword from '../app/models/ChangePassword';
import dbConfig from '../config/database';

const models = [
  Cidade,
  Pessoa,
  Usuario,
  Pagamento,
  Agendamento,
  Parceiro,
  Log,
  ChangePassword,
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(dbConfig);
    models
      .map((model) => model.init(this.connection))
      .map(
        (model) => model.associate && model.associate(this.connection.models)
      );
  }
}

export default new Database();
