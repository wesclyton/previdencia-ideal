import Sequelize, { Model } from 'sequelize';

class Cidade extends Model {
  static init(sequelize) {
    super.init(
      {
        id_cidade: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        nome: Sequelize.STRING,
        uf: Sequelize.STRING,
      },
      {
        sequelize,
        freezeTableName: 'cidades',
        tableName: 'cidades',
      }
    );

    return this;
  }
}

export default Cidade;
