import Sequelize, { Model } from 'sequelize';

class Log extends Model {
  static init(sequelize) {
    super.init(
      {
        id_log: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        modulo: Sequelize.STRING,
        tarefa: Sequelize.STRING,
        ip: Sequelize.STRING,
      },
      {
        sequelize,
        freezeTableName: 'logs',
        tableName: 'logs',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Usuario, {
      foreignKey: 'id_usuario',
      as: 'usuario',
    });
  }
}

export default Log;
