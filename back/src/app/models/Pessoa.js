import Sequelize, { DataTypes, Model } from 'sequelize';

class Pessoa extends Model {
  static init(sequelize) {
    super.init(
      {
        id_pessoa: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        nome: Sequelize.STRING,
        email: Sequelize.STRING,
        telefone: Sequelize.STRING,
        telefone2: Sequelize.STRING,
        sexo: Sequelize.CHAR,
        cpf: Sequelize.STRING,
        cnh: Sequelize.STRING,
        nascimento: Sequelize.DATEONLY,
        profissao: Sequelize.STRING,
        cep: Sequelize.STRING,
        endereco: Sequelize.STRING,
        numero: Sequelize.STRING,
        bairro: Sequelize.STRING,
        ativo: Sequelize.BOOLEAN,
        id_patrocinador: {
          type: Sequelize.INTEGER,
          field: 'id_patrocinador',
          allowNull: true,
        },
      },
      {
        sequelize,
        freezeTableName: 'pessoas',
        tableName: 'pessoas',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Cidade, {
      foreignKey: 'id_cidade',
      as: 'cidade',
    });

    this.belongsTo(models.Pessoa, {
      foreignKey: 'id_patrocinador',
      as: 'patrocinador',
    });

    this.belongsTo(models.Usuario, {
      foreignKey: 'id_usuario',
      as: 'usuario',
    });
  }
}

export default Pessoa;
