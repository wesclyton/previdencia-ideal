import Sequelize, { Model } from 'sequelize';

class Agendamento extends Model {
  static init(sequelize) {
    super.init(
      {
        id_agendamento: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        data: Sequelize.DATE,
        servico: Sequelize.STRING,
        link: Sequelize.STRING,
        estado: Sequelize.CHAR,
      },
      {
        sequelize,
        freezeTableName: 'agendamentos',
        tableName: 'agendamentos',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Pessoa, {
      foreignKey: 'id_pessoa',
      as: 'pessoa',
    });

    this.belongsTo(models.Pagamento, {
      foreignKey: 'id_pagamento',
      as: 'pagamento',
    });
  }
}

export default Agendamento;
