import Sequelize, { Model } from 'sequelize';

class Parceiro extends Model {
  static init(sequelize) {
    super.init(
      {
        id_parceiro: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        plano: Sequelize.STRING,
        estado: Sequelize.CHAR,
      },
      {
        sequelize,
        freezeTableName: 'parceiros',
        tableName: 'parceiros',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Pessoa, {
      foreignKey: 'id_pessoa',
      as: 'pessoa',
    });

    this.belongsTo(models.Pagamento, {
      foreignKey: 'id_pagamento',
      as: 'pagamento',
    });
  }
}

export default Parceiro;
