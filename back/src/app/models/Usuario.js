import Sequelize, { Model } from 'sequelize';
import bcryt from 'bcrypt';

class Usuario extends Model {
  static init(sequelize) {
    super.init(
      {
        id_usuario: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        tipo: Sequelize.CHAR,
        email: Sequelize.STRING,
        username: Sequelize.STRING,
        password: Sequelize.VIRTUAL,
        password_hash: Sequelize.STRING,
        ativo: Sequelize.BOOLEAN,
      },
      {
        sequelize,
        freezeTableName: 'usuarios',
        tableName: 'usuarios',
      }
    );

    this.addHook('beforeSave', async (usuario) => {
      if (usuario.password) {
        usuario.password_hash = await bcryt.hash(usuario.password, 8);
      }
    });

    return this;
  }

  checkPassword(password) {
    return bcryt.compare(password, this.password_hash);
  }
}

export default Usuario;
