import Sequelize, { Model } from 'sequelize';

class Pagamento extends Model {
  static init(sequelize) {
    super.init(
      {
        id_pagamento: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        tipo: Sequelize.CHAR,
        token: Sequelize.STRING,
        titulo: Sequelize.STRING,
        valor: Sequelize.DECIMAL,
        parcelas: Sequelize.INTEGER,
        forma_pagamento: Sequelize.STRING,
        estado: Sequelize.STRING,
      },
      {
        sequelize,
        freezeTableName: 'pagamentos',
        tableName: 'pagamentos',
      }
    );

    return this;
  }
}

export default Pagamento;
