import Sequelize, { Model } from 'sequelize';

class ChangePassword extends Model {
  static init(sequelize) {
    super.init(
      {
        id_change_password: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        hash: Sequelize.STRING,
        expiracao: Sequelize.DATE,
        ativo: Sequelize.BOOLEAN,
      },
      {
        sequelize,
        freezeTableName: 'change_passwords',
        tableName: 'change_passwords',
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Usuario, {
      foreignKey: 'id_usuario',
      as: 'usuario',
    });
  }
}

export default ChangePassword;
