import jwt from 'jsonwebtoken';
import * as Yup from 'yup';
import { Op } from 'sequelize';
import authConfig from '../../config/auth';

import Pessoa from '../models/Pessoa';
import Usuario from '../models/Usuario';

class SessionController {
  async session(req, res) {
    const schema = Yup.object().shape(
      {
        email: Yup.string().ensure().when('username', {
          is: '',
          then: Yup.string().email().required(),
        }),
        username: Yup.string().ensure().when('email', {
          is: '',
          then: Yup.string().required(),
        }),
        password: Yup.string().required(),
      },
      [['username', 'email']]
    );

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Falha na validação' });
    }

    const { email, username, password } = req.body;
    const usuario = await Usuario.findOne({
      where: {
        [Op.or]: [{ email }, { username }],
      },
      attributes: ['id_usuario', 'tipo', 'password_hash', 'ativo'],
    });

    if (!usuario) {
      return res.status(401).json({ error: 'Usuario não encontrado' });
    }

    if (!(await usuario.checkPassword(password))) {
      return res.status(401).json({ error: 'Senha incorreta' });
    }

    const { id_usuario, tipo, ativo } = usuario;

    if (!ativo) {
      return res.status(401).json({ error: `Usuário inativo ${ativo}` });
    }

    const pessoa = await Pessoa.findOne({
      where: {
        id_usuario,
      },
      attributes: ['id_pessoa', 'nome', 'id_patrocinador', 'ativo'],
    });

    const { nome, id_patrocinador, id_pessoa } = pessoa;

    return res.json({
      usuario: {
        id_pessoa,
        id_usuario,
        id_patrocinador,
        tipo,
        nome,
        email,
        ativo,
      },
      token: jwt.sign({ id_usuario, id_pessoa, tipo }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}

export default new SessionController();
