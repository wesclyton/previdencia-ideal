import * as Yup from 'yup';
import { pt } from 'yup-locale-pt';
import { Op } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import ChangePassword from '../models/ChangePassword';
import Usuario from '../models/Usuario';

Yup.setLocale(pt);

class ChangePasswordController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string().required().email().max(150),
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(400)
        .json({ error: 'Por favor informe o email do cadastro!' });
    }

    const { email } = req.body;

    const existsEmail = await Usuario.findOne({
      attributes: ['id_usuario', 'email'],
      where: { email },
    });

    if (!existsEmail) {
      return res.status(400).json({
        error: 'Este email não esta cadastro!',
      });
    }

    const { id_usuario } = existsEmail;

    const hash = uuidv4();

    const expiracao = new Date();
    expiracao.setHours(expiracao.getHours() + 4);

    const setBody = { hash, expiracao, ativo: true, id_usuario };

    await ChangePassword.create(setBody);
    return res.json({ email, hash });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      hash: Yup.string().required().max(150),
      password: Yup.string().required().min(6).max(20),
      confirmPassword: Yup.string()
        .required()
        .min(6)
        .max(20)
        .when('password', (password, field) =>
          password ? field.required().oneOf([Yup.ref('password')]) : field
        ),
    });

    if (!(await schema.isValid(req.body))) {
      const existeErros = await schema
        .validate(req.body)
        .catch((err) => err.errors);

      return res.status(400).json({ error: existeErros });
    }

    const { hash, password } = req.body;

    const existsHash = await ChangePassword.findOne({
      attributes: ['id_usuario', 'id_change_password'],
      where: {
        [Op.and]: [
          { hash },
          { ativo: true },
          {
            expiracao: {
              [Op.gte]: new Date(),
            },
          },
        ],
      },
    });

    if (!existsHash) {
      return res.status(400).json({
        error: 'Código invlálido ou expirado!',
      });
    }

    const { id_usuario, id_change_password } = existsHash;

    const changepassword = await ChangePassword.findByPk(id_change_password);
    const usuario = await Usuario.findByPk(id_usuario);

    const setBody = { id_usuario, password };

    try {
      await usuario.update(setBody);
      await changepassword.update({ changepassword, ativo: false });

      return res.status(200).json({ message: 'Senha atualizada com sucesso!' });
    } catch (err) {
      return res.status(401).json({ error: err });
    }
  }
}

export default new ChangePasswordController();
