import * as Yup from 'yup';
import { pt } from 'yup-locale-pt';
import Usuario from '../models/Usuario';

Yup.setLocale(pt);

class UsuarioController {
  async store(req, res) {
    const schema = Yup.object.shape({
      tipo: Yup.string().required().max(1),
      email: Yup.string().required().email().max(150),
      username: Yup.string().required().max(150),
      password: Yup.string().required().min(6).max(20),
      ativo: Yup.boolean().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Dados inválidos' });
    }

    const { id_usuario, email } = await Usuario.create(req.body);
    return res.json({ id_usuario, email });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      hash_changepassword: Yup.string().max(50),
      password: Yup.string().required().min(6).max(20),
      confirmPassword: Yup.string().required().min(6).max(20),
    });

    if (!(await schema.isValid(req.body))) {
      const existeErros = await schema
        .validate(req.body)
        .catch((err) => err.errors);

      return res.status(400).json({ error: existeErros });
    }

    const { email, username, oldPassword } = req.body;
    let pkIdUsuario;
    let reqOldPassword;

    if (req.tipoPessoa === 'A') {
      pkIdUsuario = req.body.id_usuario;
      reqOldPassword = false;
    } else {
      pkIdUsuario = req.idUsuario;
      reqOldPassword = true;
    }

    const usuario = await Usuario.findByPk(pkIdUsuario);

    if (email && email !== usuario.email) {
      const emailExists = await Usuario.findOne({
        where: { email: req.body.email },
      });

      if (emailExists) {
        return res.status(400).json({
          error: 'Este email já esta cadastro!',
        });
      }
    }

    if (username && username !== usuario.username) {
      const usernameExists = await Usuario.findOne({
        where: { username: req.body.username },
      });

      if (usernameExists) {
        return res.status(400).json({
          error: 'Esse Username já está cadastrado!',
        });
      }
    }

    if (reqOldPassword) {
      if (oldPassword && !(await usuario.checkPassword(oldPassword))) {
        return res.status(400).json({
          error: 'Senhas não correspondem!',
        });
      }
    }

    const { id_usuario, updated_at } = await usuario.update(req.body);

    return res.json({
      id_usuario,
      updated_at,
    });
  }
}

export default new UsuarioController();
