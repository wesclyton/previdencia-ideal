import * as Yup from 'yup';
import { pt } from 'yup-locale-pt';
import { Op } from 'sequelize';
import sortObj from 'sort-object';
import Pessoa from '../models/Pessoa';
import Usuario from '../models/Usuario';

Yup.setLocale(pt);

class PessoaController {
  async store(req, res) {
    const schema = Yup.object().shape({
      nome: Yup.string().required().max(150),
      email: Yup.string().email().required().max(150),
      telefone: Yup.string().required().max(20),
      telefone2: Yup.string().max(20),
      sexo: Yup.string().required().max(1),
      cpf: Yup.string().required().max(20),
      cnh: Yup.string().max(20),
      nascimento: Yup.date().required(),
      profissao: Yup.string().required().max(50),
      cep: Yup.string().required().max(10),
      endereco: Yup.string().required().max(150),
      numero: Yup.string().required().max(10),
      bairro: Yup.string().required().max(50),
      username: Yup.string().required().max(50),
      password: Yup.string().required().max(20),
      id_cidade: Yup.number().required(),
      id_patrocinador: Yup.number().required(),
    });

    if (!(await schema.isValid(req.body))) {
      const existeErros = await schema
        .validate(req.body)
        .catch((err) => err.errors);

      return res.status(400).json({ error: existeErros });
    }

    const ExistePessoa = await Pessoa.findOne({
      attributes: ['id_pessoa'],
      where: {
        [Op.or]: [{ email: req.body.email }, { cpf: req.body.cpf }],
      },
    });

    const ExisteUsername = await Usuario.findOne({
      attributes: ['id_usuario'],
      where: {
        username: req.body.username,
      },
    });

    if (ExistePessoa) {
      return res
        .status(400)
        .json({ error: 'Email ou CPF já esta cadastrado!' });
    }

    if (ExisteUsername) {
      return res.status(400).json({ error: 'Login já esta cadastrado!' });
    }

    const { email, username, password } = await req.body;

    try {
      const { id_usuario } = await Usuario.create({
        email,
        tipo: 'C',
        username,
        password,
      });

      req.body.id_usuario = id_usuario;

      const { id_pessoa, id_patrocinador, nome } = await Pessoa.create(
        req.body
      );

      return res.json({
        id_pessoa,
        id_usuario,
        id_patrocinador,
        nome,
        email,
        username,
      });
    } catch (err) {
      return res.status(400).json({
        message: `Ocorreu um erro (${err.message}). Por favor entre em contato com o administrador do sistema`,
      });
    }
  }

  async index(req, res) {
    const {
      nome,
      email,
      id_cidade,
      id_patrocinador,
      profissao,
      cpf,
      page = 1,
      limit = 10,
    } = req.body;

    const where = {};

    if (nome) where.nome = { [Op.like]: `%${nome}%` };
    if (email) where.email = email;
    if (profissao) where.profissao = profissao;
    if (cpf) where.cpf = cpf;
    if (id_cidade) where.id_cidade = id_cidade;

    if (req.tipoPessoa === 'P') where.id_patrocinador = req.idPessoa;
    else if (id_patrocinador) where.id_patrocinador = id_patrocinador;

    const pessoas = await Pessoa.findAndCountAll({
      order: [['id_pessoa', 'DESC']],
      attributes: [
        'id_pessoa',
        'nome',
        'email',
        'telefone',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'id_cidade',
        'id_patrocinador',
      ],
      where,
      limit,
      offset: (page - 1) * limit,
      include: [
        {
          association: 'cidade',
          required: true,
          attributes: ['nome', 'uf'],
        },
        {
          association: 'patrocinador',
          required: true,
          attributes: ['nome'],
          include: [
            {
              association: 'usuario',
              required: true,
              attributes: ['username'],
            },
          ],
        },
      ],
    });

    if (!pessoas) {
      return res.status(401).json({ error: 'Nenhuma pessoa encontrada' });
    }

    const pages = Math.ceil(pessoas.count / limit); // calculando numero de paginas

    pessoas.pages = pages; // incluindo numero de paginas no objeto

    try {
      return res.status(200).json(sortObj(pessoas));
    } catch (err) {
      return res.status(400).json({
        message: `Ocorreu um erro (${err.message}). Por favor entre em contato com o administrador do sistema`,
      });
    }
  }

  async details(req, res) {
    const where = {};

    if (req.tipoPessoa === 'A') where.id_pessoa = req.params.id_pessoa;
    else where.id_pessoa = req.idPessoa;

    const pessoas = await Pessoa.findAndCountAll({
      order: [['id_pessoa', 'DESC']],
      attributes: [
        'id_pessoa',
        'nome',
        'email',
        'telefone',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'id_cidade',
        'id_patrocinador',
      ],
      where,
      include: [
        {
          association: 'cidade',
          required: true,
          attributes: ['nome', 'uf'],
        },
        {
          association: 'patrocinador',
          required: true,
          attributes: ['nome'],
          include: [
            {
              association: 'usuario',
              required: true,
              attributes: ['username'],
            },
          ],
        },
      ],
    });

    if (!pessoas) {
      return res.status(401).json({ error: 'Nenhuma pessoa encontrada' });
    }

    try {
      return res.status(200).json(sortObj(pessoas));
    } catch (err) {
      return res.status(400).json({
        message: `Ocorreu um erro (${err.message}). Por favor entre em contato com o administrador do sistema`,
      });
    }
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      id_pessoa: Yup.number().required(),
      nome: Yup.string().max(150),
      email: Yup.string().email().max(150),
      telefone: Yup.string().max(20),
      telefone2: Yup.string().max(20),
      sexo: Yup.string().max(1),
      cpf: Yup.string().max(20),
      cnh: Yup.string().max(20),
      nascimento: Yup.date(),
      profissao: Yup.string().max(50),
      cep: Yup.string().max(10),
      endereco: Yup.string().max(150),
      numero: Yup.string().max(10),
      bairro: Yup.string().max(50),
      ativo: Yup.boolean(),
      username: Yup.string().max(50),
      id_cidade: Yup.number(),
      id_patrocinador: Yup.number(),
      oldPassword: Yup.string(),
      password: Yup.string()
        .min(6)
        .max(20)
        .when(
          'oldPassword',
          (oldPassword, field) =>
            oldPassword && req.tipoPessoa !== 'A' ? field.required() : field
          // se o password for preenchido e nao for admin, entao old password tem que ser preenchido
        ),
      confirmPassword: Yup.string()
        .min(6)
        .max(20)
        .when('password', (password, field) =>
          password ? field.required().oneOf([Yup.ref('password')]) : field
        ),
    });

    if (!(await schema.isValid(req.body))) {
      const existeErros = await schema
        .validate(req.body)
        .catch((err) => err.errors);

      return res.status(400).json({ error: existeErros });
    }

    const { cpf, email, username, oldPassword } = req.body;
    let pkIdUsuario;
    let pkIdPessoa;
    let reqOldPassword;

    if (req.tipoPessoa === 'A') {
      pkIdPessoa = req.body.id_pessoa;
      reqOldPassword = false;
    } else {
      pkIdPessoa = req.idPessoa;
      reqOldPassword = true;
    }

    const { id_usuario } = await Pessoa.findOne({
      where: { id_pessoa: pkIdPessoa },
    });

    if (req.tipoPessoa === 'A') pkIdUsuario = id_usuario;
    else pkIdUsuario = req.idUsuario;

    const pessoa = await Pessoa.findByPk(pkIdPessoa);
    const usuario = await Usuario.findByPk(pkIdUsuario);

    console.log(usuario.email);

    if (email && email !== usuario.email) {
      const emailExists = await Usuario.findOne({
        where: { email: req.body.email },
      });

      if (emailExists) {
        return res.status(400).json({
          error: 'Este email já esta cadastro!',
        });
      }
    }

    if (cpf && cpf !== pessoa.cpf) {
      const cpfExists = await Pessoa.findOne({
        where: { cpf: req.body.cpf },
      });

      if (cpfExists) {
        return res.status(400).json({
          error: 'CPF já cadastrado!',
        });
      }
    }

    if (username && username !== usuario.username) {
      const usernameExists = await Usuario.findOne({
        where: { username: req.body.username },
      });

      if (usernameExists) {
        return res.status(400).json({
          error: 'Esse Username já está cadastrado!',
        });
      }
    }

    if (reqOldPassword) {
      if (oldPassword && !(await usuario.checkPassword(oldPassword))) {
        return res.status(400).json({
          error: 'Senhas não correspondem!',
        });
      }
    }

    try {
      await usuario.update(req.body);
      const { updated_at } = await pessoa.update(req.body);

      return res.json({
        message: 'Dados atualizados com sucesso!',
      });
    } catch (err) {
      return res.status(400).json({
        message: `Ocorreu um erro (${err.message}). Por favor entre em contato com o administrador do sistema`,
      });
    }
  }

  /** async delete(req, res) {
    const user = await User.findByPk(req.params.id_user);

    if (!user) {
      return res.status(400).json({ error: 'User not exists!' });
    }

    await user.destroy(req.params.id_user);

    return res.json({ message: 'User excluded with success!' });
  } */
}

export default new PessoaController();
