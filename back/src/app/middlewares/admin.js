import { Op } from 'sequelize';
import Usuario from '../models/Usuario';

export default async (req, res, next) => {
  const isAdmin = await Usuario.findOne({
    where: {
      [Op.and]: {
        id_usuario: req.idUsuario,
        tipo: 'A',
      },
    },
  });

  if (!isAdmin) {
    return res
      .status(401)
      .json({ error: 'Acesso permido apenas para usuarios administradores' });
  }

  return next();
};
