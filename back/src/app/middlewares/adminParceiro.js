import { Op } from 'sequelize';
import Usuario from '../models/Usuario';

export default async (req, res, next) => {
  const isAdmParceiro = await Usuario.findOne({
    where: {
      id_usuario: req.idUsuario,
      [Op.or]: [{ tipo: 'P' }, { tipo: 'A' }],
    },
  });

  if (!isAdmParceiro) {
    return res.status(401).json({
      error: 'Acesso permido apenas para usuarios administradores e parceiros',
    });
  }

  return next();
};
