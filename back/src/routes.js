import { Router } from 'express';
import SessionController from './app/controlelrs/SessionController';
import PessoaController from './app/controlelrs/PessoaController';
import ChangePasswordController from './app/controlelrs/ChangePasswordController';
import authMiddleware from './app/middlewares/auth';
import adminMiddleware from './app/middlewares/admin';
import adminParceiroMiddleware from './app/middlewares/adminParceiro';

const routes = new Router();

routes.get('/test', (req, res) => res.json({ message: 'OK!' }));

routes.post('/recuperarsenha', ChangePasswordController.store);
routes.put('/recuperarsenha', ChangePasswordController.update);

routes.post('/session', SessionController.session);
routes.post('/pessoa', PessoaController.store);

routes.use(authMiddleware);

routes.get('/test-token', (req, res) => res.json({ message: 'OK!' }));
routes.get('/pessoas', adminParceiroMiddleware, PessoaController.index);
routes.get('/pessoas/:id_pessoa', PessoaController.details);

routes.put('/pessoas', adminMiddleware, PessoaController.update);

export default routes;
