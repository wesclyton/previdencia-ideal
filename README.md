# Previdencia Ideal

Projeto desenvolvido para Ramos & Ramos advogados.<br />
Permite o agendamento (através de um pagamento online) para consultoria online e venda de direitos de franquia.<br />
O sistema também, possui um serviço de indicação através de link para pagamento de comissão ao patrocinador.<br />


# Tecnologias

- Node.js
- REST API's
- Postgres
- Angular
- Workbench
- Yarn
- NPM

# Instalação

## Front

### Setup do Projeto
```
npm install
```
### Rodar o Projeto
```
npm start
```

## Back

### Setup do Projeto
```
npm install
```
### Rodar o Projeto
```
npm run app
```
