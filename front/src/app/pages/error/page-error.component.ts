import { Component } from '@angular/core';
import { APP_CONFIG } from '../../app.config';
import { ModuleConfig } from '../../shared/interfaces/module-config.interface';

@Component({
  selector: 'app-page-error',
  templateUrl: './page-error.component.html',
  styleUrls: ['./page-error.component.scss']
})
export class PageErrorComponent {

  get APP_CONFIG(): ModuleConfig {
    return APP_CONFIG;
  }

  constructor() {}

  voltar(): void {
    window.history.back();
  }

}
