import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  readonly loadingBS: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {}

  show(): void {
    this.loadingBS.next(true);
  }

  hide(): void {
    this.loadingBS.next(false);
  }
}
