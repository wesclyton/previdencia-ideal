import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { AuthPagesModule } from './pages/auth-pages.module';

@NgModule({
  declarations: [AuthComponent],
  imports: [
    AuthRoutingModule,
    AuthPagesModule,
    RouterModule
  ]
})
export class AuthModule {}
