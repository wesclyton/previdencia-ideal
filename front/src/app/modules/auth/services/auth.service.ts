import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { StorageService } from '../../../shared/services/storage/storage.service';
import { UsuarioLogin } from '../../usuario/models/usuario-login.interface';
import { AUTH_CONFIG } from '../auth.config';
import { Token } from '../models/token.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedBS: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private router: Router,
    private storageService: StorageService
  ) {}

  login(usuarioLogin: UsuarioLogin): Observable<Token> {
    this.isLoggedBS.next(true);
    return of({ value: 'TOKEN' });
  }

  logout(): void {
    this.storageService.localRemoveItem(AUTH_CONFIG.keyToken);
    this.isLoggedBS.next(false);
    this.router.navigateByUrl(`${AUTH_CONFIG.pathFront}/login`);
  }

  isLogged(): boolean {
    return this.getToken() ? true : false;
  }

  setTokenLocalStorage(token: any): void {
    this.storageService.localSetItem(AUTH_CONFIG.keyToken, token.accessToken);
  }

  getToken(): any {
    return this.storageService.localGetItem(AUTH_CONFIG.keyToken);
  }
}
