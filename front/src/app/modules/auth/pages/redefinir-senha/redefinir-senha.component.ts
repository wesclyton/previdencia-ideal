import { Component, ViewChild } from '@angular/core';
import { SITE_CONFIG } from '../../../../projetos/site/site.config';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { UsuarioFormRedefinirSenhaComponent } from '../../../usuario/components/form-redefinir-senha/usuario-form-redefinir-senha.component';
import { AUTH_CONFIG } from '../../auth.config';

@Component({
  selector: 'app-redefinir-senha',
  templateUrl: './redefinir-senha.component.html',
  styleUrls: ['../pages.scss']
})
export class RedefinirSenhaComponent implements PageDefault {

  pageTitle = 'Redefinir senha';

  get AUTH_CONFIG(): ModuleConfig {
    return AUTH_CONFIG;
  }

  get SITE_CONFIG(): ModuleConfig {
    return SITE_CONFIG;
  }

  @ViewChild('form', { static: false })
  form: UsuarioFormRedefinirSenhaComponent;

  constructor() {}

  onSubmit(): void {
    this.form.onSubmit();
  }

}
