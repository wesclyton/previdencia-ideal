import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UsuarioComponentModule } from '../../usuario/components/usuario-component.module';
import { CadastroComponent } from './cadastro/cadastro.component';
import { LoginComponent } from './login/login.component';
import { RedefinirSenhaComponent } from './redefinir-senha/redefinir-senha.component';

@NgModule({
  declarations: [
    LoginComponent,
    CadastroComponent,
    RedefinirSenhaComponent
  ],
  imports: [
    RouterModule,
    UsuarioComponentModule
  ]
})
export class AuthPagesModule {}
