import { Component, ViewChild } from '@angular/core';
import { SITE_CONFIG } from '../../../../projetos/site/site.config';
import { CanDeactivatePage } from '../../../../shared/interfaces/can-deactivate-page.interface';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { UsuarioFormCadastroComponent } from '../../../usuario/components/form-cadastro/usuario-form-cadastro.component';
import { AUTH_CONFIG } from '../../auth.config';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['../pages.scss'],
})
export class CadastroComponent implements PageDefault, CanDeactivatePage {

  pageTitle = 'Cadastro';

  get AUTH_CONFIG(): ModuleConfig {
    return AUTH_CONFIG;
  }

  get SITE_CONFIG(): ModuleConfig {
    return SITE_CONFIG;
  }

  @ViewChild('form', { static: false })
  form: UsuarioFormCadastroComponent;

  canDeactivateTextModal = 'Deseja cancelar o cadastro?';

  constructor() {}

  onSubmit(): void {
    this.form.onSubmit();
  }

  canDeactivate(): boolean {
   return !this.form.form.dirty || this.form.form.valid;
  }

}
