import { Component, ViewChild } from '@angular/core';
import { SITE_CONFIG } from '../../../../projetos/site/site.config';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { UsuarioFormLoginComponent } from '../../../usuario/components/form-login/usuario-form-login.component';
import { AUTH_CONFIG } from '../../auth.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../pages.scss']
})
export class LoginComponent implements PageDefault {

  pageTitle = 'Entrar';

  get AUTH_CONFIG(): ModuleConfig {
    return AUTH_CONFIG;
  }

  get SITE_CONFIG(): ModuleConfig {
    return SITE_CONFIG;
  }

  @ViewChild('form', { static: false })
  form: UsuarioFormLoginComponent;

  constructor() {}

  onSubmit(): void {
    this.form.onSubmit();
  }

}
