import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ADMIN_CONFIG } from '../../../projetos/admin/admin.config';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): boolean {
    if (this.authService.isLogged()) {
      this.router.navigateByUrl(ADMIN_CONFIG.pathFront);
      return false;
    } else {
      return true;
    }
  }
}
