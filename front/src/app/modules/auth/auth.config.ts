import { ModuleConfig } from '../../shared/interfaces/module-config.interface';

interface AuthConfig extends ModuleConfig {
  keyToken: string;
}

export const AUTH_CONFIG: AuthConfig = {
  nome: 'Autenticação',
  nomePlural: 'Autenticação',
  path: 'auth',
  pathFront: '/auth',
  keyToken: 'token'
};
