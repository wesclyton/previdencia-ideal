import { Component } from '@angular/core';
import { APP_CONFIG } from '../../app.config';
import { ModuleConfig } from '../../shared/interfaces/module-config.interface';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  get APP_CONFIG(): ModuleConfig {
    return APP_CONFIG;
  }

  constructor() {}

}
