import { ADMIN_CONFIG } from '../../projetos/admin/admin.config';
import { ModuleConfig } from '../../shared/interfaces/module-config.interface';

export const CONSULTA_CONFIG: ModuleConfig = {
  nome: 'Consulta',
  nomePlural: 'Consultas',
  path: 'consulta',
  pathFront: '/consulta',
  pathFrontAdmin: `${ADMIN_CONFIG.pathFront}/consulta`
};
