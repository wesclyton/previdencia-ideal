import { NgModule } from '@angular/core';
import { ConsultaRoutingModule } from './consulta-routing.module';
import { ConsultaPageModule } from './pages/consulta-page.module';

@NgModule({
  imports: [
    ConsultaRoutingModule,
    ConsultaPageModule
  ]
})
export class ConsultaModule {}
