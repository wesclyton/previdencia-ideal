import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultaAgendarComponent } from './pages/agendar/consulta-agendar.component';
import { ConsultaListaComponent } from './pages/lista/consulta-lista.component';

const routes: Routes = [
  {
    path: '',
    component: ConsultaListaComponent
  },
  {
    path: 'agendar',
    component: ConsultaAgendarComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class ConsultaRoutingModule {}
