import { Component } from '@angular/core';
import { PoBreadcrumb } from '@po-ui/ng-components';
import { ADMIN_CONFIG } from '../../../../projetos/admin/admin.config';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { CONSULTA_CONFIG } from '../../consulta.config';

@Component({
  selector: 'app-consulta-agendar',
  templateUrl: './consulta-agendar.component.html'
})
export class ConsultaAgendarComponent implements PageDefault {

  pageTitle = 'Agendar consulta';

  breadcrumb: PoBreadcrumb = {
    items: [
      { label: ADMIN_CONFIG.nome, link: ADMIN_CONFIG.pathFront },
      { label: CONSULTA_CONFIG.nomePlural, link: CONSULTA_CONFIG.pathFrontAdmin },
      { label: this.pageTitle }
    ]
  };

  constructor() {}

}
