import { Component } from '@angular/core';
import { PoBreadcrumb, PoPageAction } from '@po-ui/ng-components';
import { ADMIN_CONFIG } from '../../../../projetos/admin/admin.config';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { CONSULTA_CONFIG } from '../../consulta.config';

@Component({
  selector: 'app-consulta-lista',
  templateUrl: './consulta-lista.component.html'
})
export class ConsultaListaComponent implements PageDefault {

  pageTitle = CONSULTA_CONFIG.nomePlural;

  actions: Array<PoPageAction> = [
    { label: 'Agendar consulta', url: `${CONSULTA_CONFIG.pathFrontAdmin}/agendar` }
  ];

  breadcrumb: PoBreadcrumb = {
    items: [
      { label: ADMIN_CONFIG.nome, link: ADMIN_CONFIG.pathFront },
      { label: this.pageTitle }
    ]
  };

  constructor() {}

}
