import { NgModule } from '@angular/core';
import { PoPageModule } from '@po-ui/ng-components';
import { ConsultaAgendarComponent } from './agendar/consulta-agendar.component';
import { ConsultaListaComponent } from './lista/consulta-lista.component';

@NgModule({
  declarations: [
    ConsultaListaComponent,
    ConsultaAgendarComponent
  ],
  imports: [
    PoPageModule
  ]
})
export class ConsultaPageModule {}
