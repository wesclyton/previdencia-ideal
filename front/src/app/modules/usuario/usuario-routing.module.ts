import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivatePageGuard } from '../../shared/guards/can-deactivate-page.guard';
import { UsuarioAlterarSenhaComponent } from './pages/alterar-senha/usuario-alterar-senha.component';
import { UsuarioMeuCadastroComponent } from './pages/meu-cadastro/usuario-meu-cadastro.component';
import { UsuarioMinhaContaComponent } from './pages/minha-conta/usuario-minha-conta.component';
import { UsuarioMeuCadastroResolver } from './resolvers/usuario-minha-conta.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'minha-conta',
    pathMatch: 'full'
  },
  {
    path: 'minha-conta',
    component: UsuarioMinhaContaComponent
  },
  {
    path: 'meu-cadastro',
    component: UsuarioMeuCadastroComponent,
    canDeactivate: [CanDeactivatePageGuard],
    resolve: {
      usuario: UsuarioMeuCadastroResolver
    }
  },
  {
    path: 'alterar-senha',
    component: UsuarioAlterarSenhaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class UsuarioRoutingModule {}
