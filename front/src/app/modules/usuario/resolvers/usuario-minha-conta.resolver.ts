import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Usuario } from '../models/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioMeuCadastroResolver implements Resolve<Usuario> {
  constructor() {}

  resolve(): Usuario {
    return {
      municipio: '4100707',
      email: 'felipexmantovani@gmail.com',
      telefone: '44999015685',
      estado: 'PR',
      id: '123',
      nome: 'Felipe Mantovani',
      login: 'felipe'
    };
  }
}
