export interface Usuario {
  id: string;
  nome: string;
  email: string;
  telefone: string;
  estado: string;
  municipio: string;
  login: string;
}
