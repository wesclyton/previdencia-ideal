export interface UsuarioCadastro {
  id: string;
  nome: string;
  email: string;
  estado: string;
  municipio: string;
  login: string;
  password: string;
}
