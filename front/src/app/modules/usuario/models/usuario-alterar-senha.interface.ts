export interface UsuarioAlterarSenha {
  password: string;
  newPassword: string;
  confirmPassword: string;
}
