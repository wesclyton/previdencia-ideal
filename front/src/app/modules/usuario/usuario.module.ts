import { NgModule } from '@angular/core';
import { UsuarioPageModule } from './pages/usuario-page.module';
import { UsuarioRoutingModule } from './usuario-routing.module';

@NgModule({
  imports: [
    UsuarioRoutingModule,
    UsuarioPageModule
  ]
})
export class UsuarioModule {}
