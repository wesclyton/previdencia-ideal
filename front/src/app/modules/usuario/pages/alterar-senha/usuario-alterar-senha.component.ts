import { Component, ViewChild } from '@angular/core';
import { PoBreadcrumb, PoPageAction } from '@po-ui/ng-components';
import { ADMIN_CONFIG } from '../../../../projetos/admin/admin.config';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { UsuarioFormAlterarSenhaComponent } from '../../components/form-alterar-senha/usuario-form-alterar-senha.component';
import { USUARIO_CONFIG } from '../../usuario.config';

@Component({
  selector: 'app-usuario-alterar-senha',
  templateUrl: './usuario-alterar-senha.component.html'
})
export class UsuarioAlterarSenhaComponent implements PageDefault {

  pageTitle = 'Alterar senha';

  @ViewChild('form', { static: false })
  form: UsuarioFormAlterarSenhaComponent;

  actions: Array<PoPageAction> = [
    { label: 'Salvar', action: () => this.onSumit() },
    { label: 'Cancelar', url: `${USUARIO_CONFIG.pathFrontAdmin}/minha-conta` },
  ];

  breadcrumb: PoBreadcrumb = {
    items: [
      { label: ADMIN_CONFIG.nome, link: ADMIN_CONFIG.pathFront },
      { label: 'Minha conta', link: `${USUARIO_CONFIG.pathFrontAdmin}/minha-conta` },
      { label: this.pageTitle }
    ]
  };

  constructor() {}

  onSumit(): void {
    this.form.onSubmit();
  }

}
