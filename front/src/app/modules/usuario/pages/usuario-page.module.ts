import { NgModule } from '@angular/core';
import { PoButtonModule, PoPageModule, PoWidgetModule } from '@po-ui/ng-components';
import { UsuarioComponentModule } from '../components/usuario-component.module';
import { UsuarioAlterarSenhaComponent } from './alterar-senha/usuario-alterar-senha.component';
import { UsuarioMeuCadastroComponent } from './meu-cadastro/usuario-meu-cadastro.component';
import { UsuarioMinhaContaComponent } from './minha-conta/usuario-minha-conta.component';

@NgModule({
  declarations: [
    UsuarioMinhaContaComponent,
    UsuarioAlterarSenhaComponent,
    UsuarioMeuCadastroComponent
  ],
  imports: [
    UsuarioComponentModule,
    PoPageModule,
    PoWidgetModule,
    PoButtonModule
  ]
})
export class UsuarioPageModule {}
