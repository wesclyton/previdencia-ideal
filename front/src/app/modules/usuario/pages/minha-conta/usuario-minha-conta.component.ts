import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PoBreadcrumb } from '@po-ui/ng-components';
import { USUARIO_CONFIG } from '../../../../modules/usuario/usuario.config';
import { ADMIN_CONFIG } from '../../../../projetos/admin/admin.config';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { UsuarioFormCadastroComponent } from '../../components/form-cadastro/usuario-form-cadastro.component';
import { Usuario } from '../../models/usuario.interface';

@Component({
  selector: 'app-usuario-minha-conta',
  templateUrl: './usuario-minha-conta.component.html',
  styleUrls: ['./usuario-minha-conta.component.scss']
})
export class UsuarioMinhaContaComponent implements OnInit, PageDefault {

  pageTitle = 'Minha conta';

  breadcrumb: PoBreadcrumb = {
    items: [
      { label: ADMIN_CONFIG.nome, link: ADMIN_CONFIG.pathFront },
      { label: this.pageTitle }
    ]
  };

  @ViewChild('form', { static: false })
  form: UsuarioFormCadastroComponent;

  usuario: Usuario;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.usuario = this.activatedRoute.snapshot.data.usuario;
  }

  onSubmit(): void {
    this.form.onSubmit();
  }

  irParaMeuCadastro(): void {
    this.router.navigateByUrl(`${USUARIO_CONFIG.pathFrontAdmin}/meu-cadastro`);
  }

  irParaAlterarSenha(): void {
    this.router.navigateByUrl(`${USUARIO_CONFIG.pathFrontAdmin}/alterar-senha`);
  }

}
