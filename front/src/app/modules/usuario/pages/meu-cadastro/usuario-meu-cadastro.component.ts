import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PoBreadcrumb, PoPageAction } from '@po-ui/ng-components';
import { ADMIN_CONFIG } from '../../../../projetos/admin/admin.config';
import { CanDeactivatePage } from '../../../../shared/interfaces/can-deactivate-page.interface';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { UsuarioFormCadastroComponent } from '../../components/form-cadastro/usuario-form-cadastro.component';
import { Usuario } from '../../models/usuario.interface';
import { USUARIO_CONFIG } from '../../usuario.config';

@Component({
  selector: 'app-usuario-meu-cadastro',
  templateUrl: './usuario-meu-cadastro.component.html'
})
export class UsuarioMeuCadastroComponent implements OnInit, PageDefault, CanDeactivatePage {

  pageTitle = 'Meu cadastro';

  actions: Array<PoPageAction> = [
    { label: 'Salvar', action: () => this.onSubmit() },
    { label: 'Cancelar', url: `${USUARIO_CONFIG.pathFrontAdmin}/minha-conta` },
  ];

  breadcrumb: PoBreadcrumb = {
    items: [
      { label: ADMIN_CONFIG.nome, link: ADMIN_CONFIG.pathFront },
      { label: 'Minha conta', link: USUARIO_CONFIG.pathFrontAdmin },
      { label: this.pageTitle }
    ]
  };

  @ViewChild('form', { static: false })
  form: UsuarioFormCadastroComponent;

  usuario: Usuario;

  canDeactivateTextModal = 'Deseja cancelar a alteração?';

  onSubmitForm = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.usuario = this.activatedRoute.snapshot.data.usuario;
  }

  onSubmit(): void {
    this.onSubmitForm = true;
    this.form.onSubmit();
  }

  irParaAlterarSenha(): void {
    this.router.navigateByUrl(`${USUARIO_CONFIG.pathFrontAdmin}/alterar-senha`);
  }

  canDeactivate(): boolean {
    return !this.form.form.dirty || (this.onSubmitForm && !this.form.form.pristine);
  }

}
