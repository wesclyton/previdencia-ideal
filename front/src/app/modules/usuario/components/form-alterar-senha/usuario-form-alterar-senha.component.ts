import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../shared/services/notification/notification.service';
import { FormUtil } from '../../../../shared/utils/form.util';
import { AUTH_CONFIG } from '../../../auth/auth.config';
import { AuthService } from '../../../auth/services/auth.service';
import { UsuarioAlterarSenha } from '../../models/usuario-alterar-senha.interface';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-usuario-form-alterar-senha',
  templateUrl: './usuario-form-alterar-senha.component.html',
})
export class UsuarioFormAlterarSenhaComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationService: NotificationService,
    private authService: AuthService,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
    this.criarForm();
  }

  criarForm(): void {
    this.form = this.formBuilder.group({
      password: [null, [Validators.required]],
      newPassword: [null, [Validators.required]],
      confirmPassword: [null, [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.form.invalid) {
      FormUtil.validade(this.form);
      this.notificationService.error('Verifique o formulário.');
      return;
    }

    const alterarSenha: UsuarioAlterarSenha = this.form.value;
    this.usuarioService.alterarSenha(alterarSenha)
      .subscribe({
        next: () => {
          this.authService.logout();
          this.notificationService.success('Senha alterada com sucesso.');
          this.router.navigateByUrl(`${AUTH_CONFIG.pathFront}/login`);
        }
      });
  }

}
