import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { PoDividerModule, PoFieldModule } from '@po-ui/ng-components';
import { UsuarioFormAlterarSenhaComponent } from './form-alterar-senha/usuario-form-alterar-senha.component';
import { UsuarioFormCadastroComponent } from './form-cadastro/usuario-form-cadastro.component';
import { UsuarioFormLoginComponent } from './form-login/usuario-form-login.component';
import { UsuarioFormMeuCadastroComponent } from './form-meu-cadastro/usuario-form-meu-cadastro.component';
import { UsuarioFormRedefinirSenhaComponent } from './form-redefinir-senha/usuario-form-redefinir-senha.component';

@NgModule({
  declarations: [
    UsuarioFormCadastroComponent,
    UsuarioFormLoginComponent,
    UsuarioFormRedefinirSenhaComponent,
    UsuarioFormAlterarSenhaComponent,
    UsuarioFormMeuCadastroComponent
  ],
  imports: [
    ReactiveFormsModule,
    PoFieldModule,
    PoDividerModule
  ],
  exports: [
    UsuarioFormCadastroComponent,
    UsuarioFormLoginComponent,
    UsuarioFormRedefinirSenhaComponent,
    UsuarioFormAlterarSenhaComponent,
    UsuarioFormMeuCadastroComponent
  ]
})
export class UsuarioComponentModule {}
