import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PoSelectOption } from '@po-ui/ng-components';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { IbgeService } from '../../../../shared/modules/ibge/services/ibge.service';
import { NotificationService } from '../../../../shared/services/notification/notification.service';
import { FormUtil } from '../../../../shared/utils/form.util';
import { AUTH_CONFIG } from '../../../auth/auth.config';
import { Usuario } from '../../models/usuario.interface';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-usuario-form-cadastro',
  templateUrl: './usuario-form-cadastro.component.html',
})
export class UsuarioFormCadastroComponent implements OnInit, OnDestroy {

  form: FormGroup;

  estados = new Array<PoSelectOption>();

  municipios = new Array<PoSelectOption>();

  placeholderCidade = 'Cidade';

  subs = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private ibgeService: IbgeService,
    private router: Router,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
    this.criarForm();
    this.getEstados();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  criarForm(): void {
    this.form = this.formBuilder.group({
      nome: [null, [Validators.required]],
      email: [null, [Validators.required]],
      telefone: [null, [Validators.required]],
      estado: [null, [Validators.required]],
      municipio: [null, [Validators.required]],
      login: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
    this.onChangesForm();
  }

  getEstados(): void {
    this.estados = new Array<PoSelectOption>();
    this.ibgeService.getEstados().subscribe((estados) => {
      estados.forEach((estado) => {
        this.estados.push({
          label: estado.nome,
          value: estado.sigla,
        });
      });
    });
  }

  onChangesForm(): void {
    this.subs.add(
      this.form.get('estado').valueChanges.subscribe((value) => {
        if (value) {
          this.getMunicipios(value);
        }
      })
    );
  }

  getMunicipios(estado: string): void {
    this.placeholderCidade = 'Carregando...';
    this.municipios = new Array<PoSelectOption>();
    this.ibgeService
      .getMunicipios(estado)
      .pipe(finalize(() => this.placeholderCidade = 'Cidade'))
      .subscribe((municipios) => {
        municipios.forEach((municipio) => {
          this.municipios.push({
            label: municipio.nome,
            value: municipio.id,
          });
        });
      });
  }

  onSubmit(): void {
    if (this.form.invalid) {
      FormUtil.validade(this.form);
      this.notificationService.error('Verifique o formulário.');
      return;
    }

    const usuario: Usuario = this.form.value;
    this.usuarioService.create(usuario)
      .subscribe({
        next: () => {
          this.notificationService.success('Cadastro realizado com sucesso.');
          this.router.navigateByUrl(`${AUTH_CONFIG.pathFront}/login`);
        }
      });
  }

}
