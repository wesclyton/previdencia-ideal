import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ADMIN_CONFIG } from '../../../../projetos/admin/admin.config';
import { NotificationService } from '../../../../shared/services/notification/notification.service';
import { StorageService } from '../../../../shared/services/storage/storage.service';
import { FormUtil } from '../../../../shared/utils/form.util';
import { AUTH_CONFIG } from '../../../auth/auth.config';
import { AuthService } from '../../../auth/services/auth.service';
import { UsuarioLogin } from '../../models/usuario-login.interface';

@Component({
  selector: 'app-usuario-form-login',
  templateUrl: './usuario-form-login.component.html',
})
export class UsuarioFormLoginComponent implements OnInit {

  form: FormGroup;

  redirectTo: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private storageService: StorageService,
    private notificationService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.redirectTo = this.activatedRoute.snapshot.queryParams.redirectTo;
    this.criarForm();
  }

  criarForm(): void {
    this.form = this.formBuilder.group({
      login: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.form.invalid) {
      FormUtil.validade(this.form);
      this.notificationService.error('Verifique o formulário.');
      return;
    }

    const login: UsuarioLogin = this.form.value;
    this.authService.login(login).subscribe({
      next: (token) => {
        this.storageService.localSetItem(AUTH_CONFIG.keyToken, token.value);

        this.redirectTo ?
        this.router.navigateByUrl(this.redirectTo) :
        this.router.navigateByUrl(ADMIN_CONFIG.pathFront);
      }
    });
  }

}
