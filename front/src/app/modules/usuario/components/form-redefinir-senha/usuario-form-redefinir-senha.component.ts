import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../shared/services/notification/notification.service';
import { FormUtil } from '../../../../shared/utils/form.util';
import { AUTH_CONFIG } from '../../../auth/auth.config';
import { UsuarioRedefinirSenha } from '../../models/usuario-redefinir-senha.interface';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-usuario-form-redefinir-senha',
  templateUrl: './usuario-form-redefinir-senha.component.html',
})
export class UsuarioFormRedefinirSenhaComponent implements OnInit {

  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private notificationService: NotificationService,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
    this.criarForm();
  }

  criarForm(): void {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required]]
    });
  }

  onSubmit(): void {
    if (this.form.invalid) {
      FormUtil.validade(this.form);
      this.notificationService.error('Verifique o formulário.');
      return;
    }

    const redefinirSenha: UsuarioRedefinirSenha = this.form.value;
    this.usuarioService.redefinirSenha(redefinirSenha)
      .subscribe({
        next: () => {
          this.notificationService.success('Verifique a caixa de entrada do seu e-mail.');
          this.router.navigateByUrl(`${AUTH_CONFIG.pathFront}/login`);
        }
      });
  }

}
