import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PoSelectOption } from '@po-ui/ng-components';
import { Subscription } from 'rxjs';
import { IbgeService } from '../../../../shared/modules/ibge/services/ibge.service';
import { NotificationService } from '../../../../shared/services/notification/notification.service';
import { FormUtil } from '../../../../shared/utils/form.util';
import { Usuario } from '../../models/usuario.interface';
import { UsuarioService } from '../../services/usuario.service';
import { USUARIO_CONFIG } from '../../usuario.config';

@Component({
  selector: 'app-usuario-form-meu-cadastro',
  templateUrl: './usuario-form-meu-cadastro.component.html',
})
export class UsuarioFormMeuCadastroComponent implements OnInit, OnDestroy {

  form: FormGroup;

  estados = new Array<PoSelectOption>();

  municipios = new Array<PoSelectOption>();

  subs = new Subscription();

  @Input()
  usuario: Usuario;

  constructor(
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private ibgeService: IbgeService,
    private activatedRoute: ActivatedRoute,
    private usuarioService: UsuarioService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.usuario = this.activatedRoute.snapshot.data.usuario;
    this.criarForm();
    this.getEstados();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  criarForm(): void {
    this.form = this.formBuilder.group({
      nome: [null, [Validators.required]],
      email: [null, [Validators.required]],
      telefone: [null, [Validators.required]],
      estado: [null, [Validators.required]],
      municipio: [null, [Validators.required]],
      login: new FormControl({value: null, disabled: true}, Validators.required),
    });
    this.onChangesForm();

    if (this.usuario) {
      this.form.patchValue(this.usuario);
    }
  }

  getEstados(): void {
    this.estados = new Array<PoSelectOption>();
    this.ibgeService.getEstados().subscribe((estados) => {
      estados.forEach((estado) => {
        this.estados.push({
          label: estado.nome,
          value: estado.sigla,
        });
      });
      this.form.get('estado').setValue(this.usuario.estado);
    });
  }

  onChangesForm(): void {
    this.subs.add(
      this.form.get('estado').valueChanges.subscribe((value) => {
        if (value) {
          this.getMunicipios(value);
        }
      })
    );
  }

  getMunicipios(estado: string): void {
    this.municipios = new Array<PoSelectOption>();
    this.ibgeService
      .getMunicipios(estado)
      .subscribe((municipios) => {
        municipios.forEach((municipio) => {
          this.municipios.push({
            label: municipio.nome,
            value: municipio.id,
          });
        });
        this.form.get('municipio').setValue(this.usuario.municipio);
      });
  }

  onSubmit(): void {
    if (this.form.invalid) {
      FormUtil.validade(this.form);
      this.notificationService.error('Verifique o formulário.');
      return;
    }

    const usuario: Usuario = this.form.value;
    usuario.id = this.usuario.id;
    this.usuarioService.update(usuario)
      .subscribe({
        next: () => {
          this.notificationService.success('Cadastro alterado com sucesso.');
          this.router.navigateByUrl(USUARIO_CONFIG.pathFrontAdmin);
        }
      });
  }
}
