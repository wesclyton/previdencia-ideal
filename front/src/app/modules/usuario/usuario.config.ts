import { ADMIN_CONFIG } from '../../projetos/admin/admin.config';
import { ModuleConfig } from '../../shared/interfaces/module-config.interface';

export const USUARIO_CONFIG: ModuleConfig = {
  nome: 'Usuário',
  nomePlural: 'Usuários',
  path: 'usuario',
  pathFront: '/usuario',
  pathFrontAdmin: `${ADMIN_CONFIG.pathFront}/usuario`
};
