import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Usuario } from '../models/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioMinhaContaResolver implements Resolve<Usuario> {
  constructor() {}

  resolve(): Usuario {
    return {
      email: 'felipe@gmail.com',
      telefone: '44999015685',
      estado: 'PR',
      municipio: '4100707',
      id: '123',
      nome: 'Felipe Mantovani',
      login: 'felipe'
    };
  }
}
