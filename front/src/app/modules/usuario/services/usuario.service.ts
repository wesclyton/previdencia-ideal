import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { CrudGenericService } from '../../../shared/services/crud-generic/crud-generic.service';
import { ExceptionService } from '../../../shared/services/exception/exception.service';
import { UsuarioAlterarSenha } from '../models/usuario-alterar-senha.interface';
import { UsuarioRedefinirSenha } from '../models/usuario-redefinir-senha.interface';
import { Usuario } from '../models/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends CrudGenericService<Usuario> {

  constructor(
    httpClient: HttpClient,
    exceptionService: ExceptionService
  ) {
    super(
      httpClient,
      exceptionService,
      environment.api
    );
  }

  alterarSenha(usuarioAlterarSenha: UsuarioAlterarSenha): Observable<any> {
    return of(null);
  }

  redefinirSenha(usuarioRedefinirSenha: UsuarioRedefinirSenha): Observable<any> {
    return of(null);
  }

}
