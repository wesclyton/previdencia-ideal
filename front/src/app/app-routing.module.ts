import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { APP_CONFIG } from './app.config';
import { AUTH_CONFIG } from './modules/auth/auth.config';
import { AuthGuard } from './modules/auth/guards/auth.guard';
import { PageErrorComponent } from './pages/error/page-error.component';
import { ADMIN_CONFIG } from './projetos/admin/admin.config';
import { SITE_CONFIG } from './projetos/site/site.config';

const routes: Routes = [
  {
    path: APP_CONFIG.path,
    redirectTo: SITE_CONFIG.path,
    pathMatch: 'full'
  },
  {
    path: ADMIN_CONFIG.path,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    loadChildren: () => import('./projetos/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: AUTH_CONFIG.path,
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: SITE_CONFIG.path,
    loadChildren: () => import('./projetos/site/site.module').then(m => m.SiteModule)
  },
  {
    path: 'erro',
    component: PageErrorComponent
  },
  {
    path: '**',
    redirectTo: 'erro',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [PageErrorComponent],
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
