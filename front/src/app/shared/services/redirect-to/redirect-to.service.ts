import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RedirectToService {

  pathBS = new BehaviorSubject<string>('/');

  get lastPath(): string {
    return this.pathBS.value;
  }

  constructor() {}

}
