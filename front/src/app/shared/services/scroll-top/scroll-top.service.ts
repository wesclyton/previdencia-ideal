import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollTopService {

  constructor() {}

  scrollTop(elemento?: HTMLElement): void {
    if (!elemento) {
      elemento = document.getElementById('header');
    }
    if (elemento) {
      // setTimeout por causa do Firefox
      setTimeout(() => {
        const options: ScrollIntoViewOptions = {
          behavior: 'smooth'
        };
        elemento.scrollIntoView(options);
      });
    }
  }

}
