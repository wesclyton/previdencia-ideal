import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ExceptionService } from '../exception/exception.service';

@Injectable({
  providedIn: 'root'
})
export class CrudGenericService<T> {

  constructor(
    protected httpClient: HttpClient,
    protected exceptionService: ExceptionService,
    protected api: string
  ) {}

  create(type: T): Observable<T> {
    return of(null);
  }

  read(): Observable<Array<T>> {
    return of(null);
  }

  readById(id: string): Observable<T> {
    return of(null);
  }

  update(type: T): Observable<T> {
    return of(null);
  }

  delete(id: string): Observable<T> {
    return of(null);
  }

}
