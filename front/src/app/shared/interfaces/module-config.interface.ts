export interface ModuleConfig {
  nome: string;
  nomePlural: string;
  path: string;
  pathApi?: string;
  pathFront?: string;
  pathFrontAdmin?: string;
}
