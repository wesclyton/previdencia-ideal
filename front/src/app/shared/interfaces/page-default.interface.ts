import { PoBreadcrumb } from '@po-ui/ng-components';

export interface PageDefault {
  pageTitle: string;
  breadcrumb?: PoBreadcrumb;
}
