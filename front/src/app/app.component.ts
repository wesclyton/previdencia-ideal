import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AUTH_CONFIG } from './modules/auth/auth.config';
import { RedirectToService } from './shared/services/redirect-to/redirect-to.service';
import { ScrollTopService } from './shared/services/scroll-top/scroll-top.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {

  subs = new Subscription();

  constructor(
    private router: Router,
    private redirectToService: RedirectToService,
    private scrollTopService: ScrollTopService
  ) {}

  ngOnInit(): void {
    this.subs.add(
      this.router.events.subscribe((event: any) => {
        if (event instanceof NavigationStart) {
          if (event.url !== `${AUTH_CONFIG.pathFront}/login`) {
            this.redirectToService.pathBS.next(event.url);
          }

          this.scrollTopService.scrollTop();
        }
      })
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

}
