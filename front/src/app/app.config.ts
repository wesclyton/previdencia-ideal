import { environment } from '../environments/environment';
import { ModuleConfig } from './shared/interfaces/module-config.interface';

interface AppConfig extends ModuleConfig {
  api: string;
}

export const APP_CONFIG: AppConfig = {
  nome: 'Previdência Ideal',
  nomePlural: 'Previdência Ideal',
  path: '',
  pathFront: '/',
  api: environment.api
};
