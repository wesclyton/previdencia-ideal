import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SiteBeneficiosPrevidenciariosComponent } from './pages/beneficios-previdenciarios/site-beneficios-previdenciarios.component';
import { SiteCalculosPrevidenciariosComponent } from './pages/calculos-previdenciarios/site-calculos-previdenciarios.component';
import { SiteHomeComponent } from './pages/home/site-home.component';
import { SiteRevisoesPrevidenciariasComponent } from './pages/revisoes-previdenciarias/site-revisoes-previdenciarias.component';
import { SiteSejaUmParceiroComponent } from './pages/seja-um-parceiro/site-seja-um-parceiro.component';
import { SiteComponent } from './site.component';

const routes: Routes = [
  {
    path: '',
    component: SiteComponent,
    children: [
      {
        path: '',
        component: SiteHomeComponent
      },
      {
        path: 'seja-um-parceiro',
        component: SiteSejaUmParceiroComponent
      },
      {
        path: 'beneficios-previdenciarios',
        component: SiteBeneficiosPrevidenciariosComponent
      },
      {
        path: 'revisoes-previdenciarias',
        component: SiteRevisoesPrevidenciariasComponent
      },
      {
        path: 'calculos-previdenciarios',
        component: SiteCalculosPrevidenciariosComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class SiteRoutingModule {}
