import { ModuleConfig } from '../../shared/interfaces/module-config.interface';

export const SITE_CONFIG: ModuleConfig = {
  nome: 'Site',
  nomePlural: 'Site',
  path: 'site',
  pathFront: '/site'
};
