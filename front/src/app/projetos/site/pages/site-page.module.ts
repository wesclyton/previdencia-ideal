import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SiteComponentModule } from '../components/site-component.module';
import { SiteBeneficiosPrevidenciariosComponent } from './beneficios-previdenciarios/site-beneficios-previdenciarios.component';
import { SiteCalculosPrevidenciariosComponent } from './calculos-previdenciarios/site-calculos-previdenciarios.component';
import { SiteHomeComponent } from './home/site-home.component';
import { SiteRevisoesPrevidenciariasComponent } from './revisoes-previdenciarias/site-revisoes-previdenciarias.component';
import { SiteSejaUmParceiroComponent } from './seja-um-parceiro/site-seja-um-parceiro.component';

@NgModule({
  declarations: [
    SiteHomeComponent,
    SiteSejaUmParceiroComponent,
    SiteBeneficiosPrevidenciariosComponent,
    SiteRevisoesPrevidenciariasComponent,
    SiteCalculosPrevidenciariosComponent
  ],
  imports: [
    SiteComponentModule,
    RouterModule
  ]
})
export class SitePageModule {}
