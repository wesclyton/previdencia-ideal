import { Component } from '@angular/core';
import { AUTH_CONFIG } from '../../../../modules/auth/auth.config';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';

@Component({
  selector: 'app-site-seja-um-parceiro',
  templateUrl: './site-seja-um-parceiro.component.html',
  styleUrls: ['./site-seja-um-parceiro.component.scss']
})
export class SiteSejaUmParceiroComponent {

  get AUTH_CONFIG(): ModuleConfig {
    return AUTH_CONFIG;
  }

  constructor() {}

}
