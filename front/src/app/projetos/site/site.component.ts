import { Component, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { APP_CONFIG } from '../../app.config';
import { ScrollTopService } from '../../shared/services/scroll-top/scroll-top.service';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.scss']
})
export class SiteComponent {

  scrollTopActive = false;

  constructor(
    private scrollTopService: ScrollTopService,
    private title: Title
  ) {
    this.title.setTitle(APP_CONFIG.nome);
  }

  scrollTop(): void {
    this.scrollTopService.scrollTop();
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event: any): void {
    const top = event.target.scrollingElement.scrollTop;
    top >= 200 ? this.scrollTopActive = true : this.scrollTopActive = false;
  }

}
