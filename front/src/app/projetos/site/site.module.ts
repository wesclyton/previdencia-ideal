import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SiteComponentModule } from './components/site-component.module';
import { SitePageModule } from './pages/site-page.module';
import { SiteRoutingModule } from './site-routing.module';
import { SiteComponent } from './site.component';

@NgModule({
  declarations: [SiteComponent],
  imports: [
    CommonModule,
    RouterModule,
    SiteRoutingModule,
    SiteComponentModule,
    SitePageModule
  ]
})
export class SiteModule {}
