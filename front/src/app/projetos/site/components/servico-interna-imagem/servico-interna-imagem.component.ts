import { AfterViewInit, Component, ElementRef, Input, Renderer2, ViewChild } from '@angular/core';
import { CONSULTA_CONFIG } from '../../../../modules/consulta/consulta.config';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';

@Component({
  selector: 'app-servico-interna-imagem',
  templateUrl: './servico-interna-imagem.component.html',
  styleUrls: ['./servico-interna-imagem.component.scss']
})
export class SiteServicoInternaImagemComponent implements AfterViewInit {

  @Input()
  imagem: string;

  @ViewChild('boxImagem', { static: false })
  boxImagem: ElementRef;

  get CONSULTA_CONFIG(): ModuleConfig {
    return CONSULTA_CONFIG;
  }

  esconderBox = false;

  constructor(
    private renderer: Renderer2
  ) {}

  ngAfterViewInit(): void {
    this.imagem = `assets/site/${this.imagem}`;
    this.renderer.setStyle(this.boxImagem.nativeElement, 'background-image', `url(${this.imagem})`);
  }

  esconder(): void {
    this.esconderBox = !this.esconderBox;
  }

}
