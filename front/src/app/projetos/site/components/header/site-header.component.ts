import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { APP_CONFIG } from '../../../../app.config';
import { AUTH_CONFIG } from '../../../../modules/auth/auth.config';
import { AuthService } from '../../../../modules/auth/services/auth.service';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';
import { ScrollTopService } from '../../../../shared/services/scroll-top/scroll-top.service';
import { ADMIN_CONFIG } from '../../../admin/admin.config';
import { SITE_CONFIG } from '../../site.config';

@Component({
  selector: 'app-site-header',
  templateUrl: './site-header.component.html',
  styleUrls: ['./site-header.component.scss']
})
export class SiteHeaderComponent implements OnInit, OnDestroy, AfterViewInit {

  menuAtivo = false;

  get APP_CONFIG(): ModuleConfig {
    return APP_CONFIG;
  }

  get ADMIN_CONFIG(): ModuleConfig {
    return ADMIN_CONFIG;
  }

  get SITE_CONFIG(): ModuleConfig {
    return SITE_CONFIG;
  }

  get AUTH_CONFIG(): ModuleConfig {
    return AUTH_CONFIG;
  }

  subs = new Subscription();

  @ViewChildren('linkOnePage')
  linksOnePage: QueryList<ElementRef>;

  usuarioEstaLogado = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private scrollTopService: ScrollTopService
  ) {}

  ngOnInit(): void {
    this.subs.add(this.authService.isLoggedBS.subscribe((isLogado) => this.usuarioEstaLogado = this.authService.isLogged() || isLogado));
  }

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {
    this.linksOnePage.forEach(link => link.nativeElement.addEventListener('click', this.onClickLink.bind(this)));
  }

  mostraMenu(): void {
    this.menuAtivo = !this.menuAtivo;
  }

  onClickLink(event: PointerEvent): void {
    event.preventDefault();
    const elemento = event.target as HTMLAnchorElement;
    const section = this.getSection(elemento);
    section && section.id !== 'header' ? this.navegarComScroll(section) : this.navegarNormal(elemento);
    this.mostraMenu();
  }

  getSection(elemento: HTMLAnchorElement): HTMLElement {
    return document.getElementById(elemento.hash.split('#')[1]) as HTMLElement;
  }

  navegarComScroll(section: HTMLElement): void {
    this.scrollTopService.scrollTop(section);
  }

  navegarNormal(elemento: HTMLAnchorElement): void {
    this.router.navigateByUrl(SITE_CONFIG.pathFront);
    setTimeout(() => {
      const section = this.getSection(elemento);
      if (section) {
        this.navegarComScroll(section);
      }
    }, 250);
  }

}
