import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SiteFooterComponent } from './footer/site-footer.component';
import { SiteHeaderComponent } from './header/site-header.component';
import { SiteServicoInternaImagemComponent } from './servico-interna-imagem/servico-interna-imagem.component';
import { ServicoItemListaComponent } from './servico-item-lista/servico-item-lista.component';
import { SiteServicoComponent } from './servico/site-servico.component';

@NgModule({
  declarations: [
    SiteHeaderComponent,
    SiteFooterComponent,
    ServicoItemListaComponent,
    SiteServicoComponent,
    SiteServicoInternaImagemComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    SiteHeaderComponent,
    SiteFooterComponent,
    ServicoItemListaComponent,
    SiteServicoComponent,
    SiteServicoInternaImagemComponent
  ]
})
export class SiteComponentModule {}
