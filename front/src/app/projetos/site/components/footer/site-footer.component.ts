import { Component } from '@angular/core';
import { APP_CONFIG } from '../../../../app.config';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';

@Component({
  selector: 'app-site-footer',
  templateUrl: './site-footer.component.html',
  styleUrls: ['./site-footer.component.scss']
})
export class SiteFooterComponent {

  get APP_CONFIG(): ModuleConfig {
    return APP_CONFIG;
  }

  get anoCorrente(): number {
    return new Date().getFullYear();
  }

  constructor() {}
}
