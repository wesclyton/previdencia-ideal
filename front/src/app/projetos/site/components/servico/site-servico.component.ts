import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-site-servico',
  templateUrl: './site-servico.component.html',
  styleUrls: ['./site-servico.component.scss']
})
export class SiteServicoComponent {

  @Input()
  titulo: string;

  constructor() {}

}
