import { AfterViewInit, Component, ElementRef, Input, Renderer2, ViewChild } from '@angular/core';
import { ModuleConfig } from '../../../../shared/interfaces/module-config.interface';
import { SITE_CONFIG } from '../../site.config';

@Component({
  selector: 'app-servico-item-lista',
  templateUrl: './servico-item-lista.component.html',
  styleUrls: ['./servico-item-lista.component.scss']
})
export class ServicoItemListaComponent implements AfterViewInit {

  get SITE_CONFIG(): ModuleConfig {
    return SITE_CONFIG;
  }

  @Input()
  imagem: string;

  @Input()
  titulo: string;

  @Input()
  rota: string;

  @ViewChild('box', { static: false })
  box: ElementRef;

  constructor(
    private renderer: Renderer2
  ) {}

  ngAfterViewInit(): void {
    this.imagem = `assets/site/${this.imagem}`;
    this.renderer.setStyle(this.box.nativeElement, 'background-image', `url(${this.imagem})`);
  }

}
