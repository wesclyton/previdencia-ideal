import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoDialogConfirmOptions, PoDialogService, PoMenuItem, PoToolbarAction, PoToolbarProfile } from '@po-ui/ng-components';
import { APP_CONFIG } from '../../app.config';
import { AuthService } from '../../modules/auth/services/auth.service';
import { CONSULTA_CONFIG } from '../../modules/consulta/consulta.config';
import { USUARIO_CONFIG } from '../../modules/usuario/usuario.config';
import { ModuleConfig } from '../../shared/interfaces/module-config.interface';
import { StringUtil } from '../../shared/utils/string.util';
import { SITE_CONFIG } from '../site/site.config';
import { ADMIN_CONFIG } from './admin.config';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, AfterViewInit {

  logo = './assets/admin/logo.svg';

  logoSimbolo = './assets/admin/logo-simbolo.svg';

  maxShortLabel = 5;

  profile: PoToolbarProfile = {
    title: 'Olá, Felipe!',
  };

  toolbarActions: Array<PoToolbarAction> = [
    {
      label: 'Minha conta',
      icon: 'po-icon-user',
      url: USUARIO_CONFIG.pathFrontAdmin
    },
    {
      label: 'Voltar ao site',
      icon: 'po-icon-first-page',
      action: () => this.voltarAoSite()
    },
    {
      label: 'Sair',
      icon: 'po-icon-exit',
      type: 'danger',
      separator: true,
      action: () => this.sair()
    }
  ];

  menu: Array<PoMenuItem>;

  get APP_CONFIG(): ModuleConfig {
    return APP_CONFIG;
  }

  get SITE_CONFIG(): ModuleConfig {
    return SITE_CONFIG;
  }

  get ADMIN_CONFIG(): ModuleConfig {
    return ADMIN_CONFIG;
  }

  constructor(
    private router: Router,
    private authService: AuthService,
    private poDialogService: PoDialogService
  ) {}

  ngOnInit(): void {
    this.getMenu();
  }

  ngAfterViewInit(): void {
    const containerLogo = document.getElementsByClassName('po-menu-header-container-logo')[0] as HTMLElement;
    const linkLogo = containerLogo.firstElementChild;
    linkLogo.setAttribute('href', ADMIN_CONFIG.pathFront);
  }

  getMenu(): void {
    this.menu = new Array<PoMenuItem>();

    this.menu.push(
      {
        label: 'Minha conta',
        link: `${USUARIO_CONFIG.pathFrontAdmin}/minha-conta`,
        icon: 'po-icon-user',
        shortLabel: StringUtil.resume('Minha conta', this.maxShortLabel, true)
      },
      {
        label: 'Dashboard',
        link: `${ADMIN_CONFIG.pathFront}/dashboard`,
        icon: 'po-icon-grid',
        shortLabel: StringUtil.resume('Dashboard', this.maxShortLabel, true)
      },
      {
        label: CONSULTA_CONFIG.nomePlural,
        link: CONSULTA_CONFIG.pathFrontAdmin,
        icon: 'po-icon-calendar',
        shortLabel: StringUtil.resume(CONSULTA_CONFIG.nomePlural, this.maxShortLabel, true)
      }
    );
  }

  sair(): void {
    const options: PoDialogConfirmOptions = {
      title: 'Confirmação!',
      message: 'Realmente deseja sair?',
      confirm: () => {
        this.authService.logout();
      },
      cancel: () => {}
    };
    this.poDialogService.confirm(options);
  }

  voltarAoSite(): void {
    this.router.navigateByUrl(SITE_CONFIG.pathFront);
  }

}
