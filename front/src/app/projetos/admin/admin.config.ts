import { ModuleConfig } from '../../shared/interfaces/module-config.interface';

export const ADMIN_CONFIG: ModuleConfig = {
  nome: 'Meu painel',
  nomePlural: 'Meu painel',
  path: 'admin',
  pathFront: '/admin'
};
