import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PoAvatarModule, PoButtonModule, PoMenuModule, PoPopupModule, PoToolbarModule } from '@po-ui/ng-components';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminPageModule } from './pages/admin-page.module';

@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    AdminRoutingModule,
    AdminPageModule,
    PoMenuModule,
    PoButtonModule,
    PoAvatarModule,
    PoPopupModule,
    PoToolbarModule
  ]
})
export class AdminModule {}
