import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CONSULTA_CONFIG } from '../../modules/consulta/consulta.config';
import { USUARIO_CONFIG } from '../../modules/usuario/usuario.config';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: USUARIO_CONFIG.path,
        loadChildren: () => import('../../modules/usuario/usuario.module').then(m => m.UsuarioModule)
      },
      {
        path: CONSULTA_CONFIG.path,
        loadChildren: () => import('../../modules/consulta/consulta.module').then(m => m.ConsultaModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class AdminRoutingModule {}
