import { Component } from '@angular/core';
import { PoBreadcrumb } from '@po-ui/ng-components';
import { PageDefault } from '../../../../shared/interfaces/page-default.interface';
import { ADMIN_CONFIG } from '../../admin.config';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements PageDefault {

  pageTitle = 'Dashboard';

  breadcrumb: PoBreadcrumb = {
    items: [
      { label: ADMIN_CONFIG.nome, link: ADMIN_CONFIG.pathFront },
      { label: this.pageTitle }
    ]
  };

  constructor() {}

}
