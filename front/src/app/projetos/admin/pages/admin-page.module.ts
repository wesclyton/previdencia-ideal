import { NgModule } from '@angular/core';
import { PoPageModule } from '@po-ui/ng-components';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    PoPageModule
  ]
})
export class AdminPageModule {}
